const supported_packages = [
    :CSVFiles,
    :DataFrames,
    :Gadfly,
    :HypothesisTests,
    :NamedArrays,
    :Plots,
    :Symbolics,
]

# Generic fallback
orgshow(io::IO, Any, i; kwargs...) = show(io, i)
orgshow(io::IO, ::MIME"text/org", i; kwargs...) = show(io, i)
# Overload types
orgshow(io::IO, ::MIME"text/org", t::Tuple; kwargs...) = print(io, join(t, ','))
orgshow(io::IO, ::MIME"text/org", ::Nothing; kwargs...) = print(io, "")
orgshow(io::IO, ::MIME"text/org", a::Array{T,1}; kwargs...) where T <: Any = print(io, join(a, '\n'))

# You can override this with a better one that uses some available module
function orgshow(io::IO, ::MIME"text/html", i::Array{T,2}; kwargs...) where T <: Any
    width = get(Dict(kwargs), :width, "100")
    print(io, """<table style="width:$width%">""")
    content = eachrow(i) |> x -> string("<tr>",
                                        join([string("<th>", join(l, "</th><th>"))
                                       for l in x], "</tr><tr>"))
    print(io, content, "</table>")
end

function orgshow(io::IO, ::MIME"text/org", i::Array{T,2}; kwargs...) where T <: Any
    out = eachrow(i) |> x -> join([join(l, ',') for l in x], '\n')
    print(io, out)
end

function orgshow(io::IO, ::MIME"text/csv", i::Array{T,2}; kwargs...) where T <: Any
    orgshow(io, MIME("text/org"), i; kwargs...)
end

function orgstring(e::Tuple{Exception,Any})
    string("ERROR,", e[1], "\n",
           "Stacktrace:\n",
           join(e[2], '\n'))
end

# The comma is needed to allow export as table
function orgshow(io::IO, ::MIME"text/org", e::Tuple{Exception,Any};
                 kwargs...)
    print(io, orgstring(e))
end

function orgshow(io::IO, ::MIME"text/org", t::NamedTuple)
    print(io, join(string.(keys(t)), ','))
    println(io)
    print(io, join(t, ','))
end

function orgshow(io::IO, ::MIME"text/org",
                          ta::Vector{<:NamedTuple})
    "This assume keys are the same. A better NamedTuple export is provided by
             the DataFrames (DataFrame(ta))"
    length(ta) <= 0 && return ""
    println(io, join(keys(first(ta)), ','))
    for t in ta
        print(io, join(string.(values(t)), ','))
        println(io)
    end
end

OrgAPApvalue(p) = p < 0.001 ? "< .001" : string("= ", round(p, sigdigits = 2))

function define_HypothesisTests()
    Main.@eval function orgshow(io::IO,
                                m::MIME"text/org",
                                test::PowerDivergenceTest; kwargs...)
        println(io, join(["test", "df", "N", "stat", "p-value"], ','))
        print(io, join(["X^2", test.df, test.n, test.stat, pvalue(test)], ','))
    end
    Main.@eval function orgshow(io::IO,
                                m::MIME"text/html",
                                test::PowerDivergenceTest; kwargs...)
        print(io, string("X^2",
                         " (", test.df, ", N = ", test.n,
                         ") = ",
                         round(test.stat; digits = 2),
                         ", p ", OrgAPApvalue(pvalue(test))))
    end
end

function define_Symbolics()
    Main.@eval function orgshow(io::IO,
                                m::MIME"application/x-tex",
                                expr::Symbolics.Num; kwargs...)
        Main.@eval using Latexify
        print(io, latexify(expr, starred=true))
    end
end

function define_Gadfly()
    Main.@eval function orgshow(io::IO,
                                m::MIME"image/svg+xml",
                                p::Gadfly.Plot; kwargs...)
        draw(SVG(io, 4inch, 3inch), p)
    end
    Main.@eval function orgshow(io::IO,
                                m::MIME"image/png",
                                p::Gadfly.Plot; kwargs...)
        draw(PNG(io, 4inch, 3inch, dpi=600), p)
    end
end

function define_Plots()
    # Fallback: we will try to plot any image/png or image/svg
    Main.@eval function orgshow(io::IO,
                                m::MIME"image/png",
                                any; kwargs...)
        show(io, MIME("image/png"), plot(any; kwargs...))
    end
    Main.@eval function orgshow(io::IO,
                                m::MIME"image/svg+xml",
                                any; kwargs...)
        show(io, MIME("image/svg+xml"), plot(any; kwargs...))
    end
    Main.@eval function orgshow(io::IO,
                                m::MIME"image/png",
                                p::Plots.Plot; kwargs...)
        show(io, MIME("image/png"), plot(p; kwargs...))
    end
    Main.@eval function orgshow(io::IO, ::MIME"image/png", e::Tuple{Exception,Any};
                                kwargs...)
        let p = plot(showaxis = false, grid = false, bg = :yellow)
            annotate!([0.5], [0.5], (orgstring(e), :red))
            orgshow(io, MIME("image/png"), p; kwargs...)
        end
    end
    Main.@eval function orgshow(io::IO, ::MIME"image/svg+xml", e::Exception; kwargs...)
        let p = plot(showaxis = false, grid = false, bg = :yellow)
            annotate!([0.5], [0.5], (string("ERROR: ", e), :red))
            orgshow(io, MIME("image/svg+xml"), p; kwargs...)
        end
    end
    Main.@eval function orgshow(io::IO, ::MIME"text/html", p::Plots.Plot; kwargs...)
        p = plot(p; kwargs...)
        p.attr[:html_output_format] = "png"
        # Plots._show(io::IO, MIME("text/html"), p::Plots.Plot; kwargs...)
        Plots._show(io, MIME("text/html"), p)
    end
    Main.@eval function orgshow(io::IO, ::MIME"image/svg+xml", p::Plots.Plot; kwargs...)
        show(io, MIME("image/svg+xml"), plot(p; kwargs...))
    end
    Main.@eval function orgshow(io::IO, ::MIME"application/pdf", p::Plots.Plot; kwargs...)
        # ps, eps, tex or pdf. I think extra packages are required for all but pdf
        show(io, MIME("application/pdf"), plot(p; kwargs...))
    end
    Main.@eval function orgshow(io::IO, ::MIME"application/postscript", p::Plots.Plot; kwargs...)
        show(io, MIME("application/postscript"), plot(p; kwargs...))
    end
    Main.@eval function orgshow(io::IO, ::MIME"image/eps", p::Plots.Plot;
                                kwargs...)
        show(io, MIME("image/eps"), plot(p; kwargs...))
    end
    Main.@eval function orgshow(io::IO, ::MIME"application/x-tex", p::Plots.Plot;
                                kwargs...)
        show(io, MIME("application/x-tex"), plot(p; kwargs...))
    end
    Main.@eval function orgshow(io::IO, ::MIME"text/org", p::Plots.Plot; kwargs...)
        # png or svg
        p.attr[:html_output_format] = "png"
        orgshow(io::IO, MIME("text/html"), p::Plots.Plot; kwargs...)
    end
end

function define_DataFrames()
    Main.@eval function orgshow(io::IO, ::MIME"text/csv", d::DataFrames.DataFrame)
        orgshow(io, MIME("text/org"), d)
    end
    Main.@eval function orgshow(io::IO, ::MIME"text/org", d::DataFrames.DataFrame)
        out = join(string.(names(d)), ',') * '\n'
        out *= join([join(x, ',') for x in eachrow(d) .|> collect],'\n')
        print(io, out)
    end
end

function define_CSVFiles()
    Main.@eval function orgshow(io::IO, ::MIME"text/csv", d::CSVFiles.CSVFile)
        orgshow(io, MIME("text/org"), d)
    end
    Main.@eval function orgshow(io::IO, ::MIME"text/html", d::CSVFiles.CSVFile)
        show(io, MIME("text/html"), RES)
    end
    Main.@eval function orgshow(io::IO, ::MIME"application/json", d::CSVFiles.CSVFile)
        show(io, MIME("application/vnd.dataresource+json"), RES)
    end
    Main.@eval function orgshow(io::IO, ::MIME"text/org", d::CSVFiles.CSVFile)
        orgshow(io, MIME("text/org"), collect(d))
    end
end

function define_NamedArrays()
    Main.@eval function orgshow(io::IO, ::MIME"text/org",
                                 na::NamedArray{T,2} where T <: Any)
        n = names(na)
        a = collect(na)
        # The char used by NamedArrays is '╲' but by default it's not
        # shown in pdf export
        print(io, join(string.(na.dimnames), " \\ ") * ',')
        print(io, join(n[2], ',') * '\n')
        print(io, join([join([string(n[1][i], ','),
                              join([a[i,j]
                                    for j in 1:size(na,2)
                                    ], ',')])
                        for i in 1:size(na,1)
                        ], '\n'))
    end
    Main.@eval function orgshow(io::IO, ::MIME"text/org", na::NamedArray{T,1} where T <: Any)
        n = names(na)
        a = collect(na)
        print(io, string(na.dimnames[1], ',', '\n'))
        print(io, join([join([n[1][i], a[i]], ',')
                        for i in 1:length(n[1])], '\n'))
    end
end

function OrgBabelReload()
    "Defines show method based on loaded packages"
    for pkg in supported_packages
        if isdefined(Main, pkg) && (isa(getfield(Main, pkg), Module) ||
                                    isa(getfield(Main, pkg), UnionAll))
            eval(Symbol("define_", pkg))()
            # Remove loaded packages from list to prevent multiple execution
            filter!(x -> x != pkg, supported_packages)
        end
    end
end

const Mimes = Dict(:org => "text/org",
                   :csv => "text/csv",
                   :txt => "text/plain",
                   :png => "image/png",
                   :svg => "image/svg+xml",
                   :pdf => "application/pdf",
                   :html => "text/html",
                   :auto => "text/org",
                   :ps => "application/postscript",
                   :eps => "image/eps",
                   :tex => "application/x-tex")

function OrgBabelFormat(src_file::String,       # Le path du fichier à exécuter
                        dir::String,            # l'endroit où le runner
                        vars_file::String,      # les variables à lier avant
                        eval_type::String,      # :value ou :output
                        output_file::String,    # Le fichier où écrire le résultat
                        output_format::String,  # Une clé dans `Dict`
                        pure::Bool,
                        kwargs)# What are kwargs??
    content = read(src_file, String)
    vars = read(vars_file, String)
    # Fake a prompt with the current input
    try
        printstyled(IOContext(stdout, :color => true),
                    "\njulia> ", color = :blue)
        println(content)
    catch
    end

    value_or_output = Symbol(eval_type)

    # Dispatch on output type
    code = pure ? "let $vars; $content; end" : "begin $vars; $content;\n end"
    if value_or_output == :value
        # Run the code
        result = cd(expanduser(dir)) do
            try
                Main.eval(Meta.parse(code))
            catch e
                (e, stacktrace())
            end
        end
        try
            display(MIME("text/plain"), result)
        catch e
            println("Error $e while showing results")
        end

        # Decide output type. If we were prescribed a mime type, use it. If we
        # can guess from the output's extension, use this one. Fallback to
        # text/org.

        fmt = Symbol(output_format)
        if fmt == :auto
            # on tente de le remplacer par quelque chose qui marche avec
            # l'extension de fichier output.
            output_ext = replace(splitext(output_file)[2], "." => "")
            if !isempty(output_ext)
                fmt = Symbol(output_ext)
            end
        end

        # TODO: do not fail silently when no suitable type is found
        mime = get(Mimes, fmt, nothing)
        if isnothing(mime)
            error("invalid output format: $fmt")
        end
        temporary_output = IOBuffer()
        # Output directly to org (no :file, -> save to output_file)
        OrgBabelReload()
        try
            Base.@invokelatest orgshow(temporary_output,
                                       MIME(mime),
                                       result;
                                       Base.eval(Meta.parse(kwargs))...)
        catch e
            @error "Probable ob-julia error! Please report to the author!"
            @error "Error: $e"
            print(temporary_output,
                  "Probable ob-julia error! Please report to the author!",
                  "Error: $e")
        end
        write(output_file, take!(temporary_output))
    elseif value_or_output == :output
        temporary_output_file = tempname()
        open(temporary_output_file, create = true, write = true) do f
            redirect_stdout(f) do
                redirect_stderr(f) do
                    cd(expanduser(dir)) do
                        try
                            Main.eval(Meta.parse(code))
                        catch e
                            println(e)
                            println(join(stacktrace(), '\n'))
                        end
                    end
                end
            end
        end
        mv(temporary_output_file, output_file, force = true)
    else
        "ERROR: invalid ouput type"
    end
    return nothing
end

